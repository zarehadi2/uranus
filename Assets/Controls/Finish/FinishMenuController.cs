﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishMenuController : MonoBehaviour
{
    public static FinishMenuController instance;

    public GameObject TouchPad;

    void Awake()
    {
        if (instance != null)
            Destroy(instance.gameObject);
        instance = this;

        gameObject.SetActive(false);
    }

    public void BtnNext()
    {
        int idx = SceneManager.GetActiveScene().buildIndex;
        idx++;
        if (idx >= SceneManager.sceneCountInBuildSettings)
        {
            idx = 0;
        }
        Time.timeScale = 1f;
        SceneManager.LoadScene(idx);
    }

    public void BtnRetry()
    {
        Time.timeScale = 1f;
        LevelManager.Instance.KillPlayer();
        gameObject.SetActive(false);
        //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void BtnExit()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Main");
    }
}
