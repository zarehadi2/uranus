﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseBtnController : MonoBehaviour
{
    public static PauseBtnController instance;

    public GameObject[] disableOnPause;
    public GameObject[] enableOnPause;

    void Awake()
    {
        if (instance != null)
            Destroy(instance.gameObject);
        instance = this;
    }

    public void BtnPause()
    {
        var b = disableOnPause[0].activeInHierarchy;
        foreach (var gm in disableOnPause)
            gm.SetActive(false);
        foreach (var gm in enableOnPause)
            gm.SetActive(true);
        //PausePageController.instance.enableTouchPadOrNot = b;
        Time.timeScale = 0f;
    }

    public void btnNext()
    {
        int idx = SceneManager.GetActiveScene().buildIndex;
        idx++;
        if (idx >= 5)
        {
            idx = 0;
        }
        SceneManager.LoadScene(idx);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            BtnPause();
        }
    }
}
