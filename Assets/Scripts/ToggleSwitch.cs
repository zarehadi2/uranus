﻿using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[Serializable]
public class ValueChanged : UnityEvent { }

[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(AudioSource))]
public class ToggleSwitch : MonoBehaviour, IPointerDownHandler
{
    [SerializeField]
    private bool _isOn = false;
    public bool isOn
    {
        get
        {
            return _isOn;
        }
    }

    [SerializeField] private RectTransform toggleIndicator;
    [SerializeField] private RectTransform toggleBackground;
    private float onX;
    private float offX;

    private AudioSource audioSource;
    private float tweenTime = .25f;

    [SerializeField] public ValueChanged valueChanged;

    void Start()
    {
        offX = toggleIndicator.anchoredPosition.x;
        onX = toggleBackground.rect.width - toggleIndicator.anchoredPosition.x - toggleIndicator.rect.width;
        audioSource = GetComponent<AudioSource>();
    }

    void OnEnable()
    {
        Toggle(isOn);
    }

    private void Toggle(bool value, bool playSFX = true)
    {
        if (_isOn != value)
        {
            _isOn = value;

            MoveIndicator(isOn);

            if (playSFX)
                audioSource.Play();

            if (valueChanged != null)
                valueChanged.Invoke();

        }
    }

    private void MoveIndicator(bool value)
    {
        if (value)
            toggleIndicator.DOAnchorPosX(onX, tweenTime);
        else
            toggleIndicator.DOAnchorPosX(offX, tweenTime);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Toggle(!isOn);
    }
}
