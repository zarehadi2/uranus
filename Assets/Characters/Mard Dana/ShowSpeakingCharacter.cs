﻿using System.Collections;
using UnityEngine;

public class ShowSpeakingCharacter : MonoBehaviour
{
    public static ShowSpeakingCharacter instance;

    public GameObject TouchPad;
    public GameObject characterToShow;
    public GameObject PauseBtn;
    public bool isShow = false;

    public bool hasForm;

    public AudioSource audioSource;

    Transform lastParent;

    void Awake()
    {
        instance = this;
    }

    void OnEnable()
    {
        if (isShow)
            show();
        else
            hide();
    }

    void OnDisable()
    {
        if (!isShow)
            hide();
    }

    public void show()
    {
        foreach (Transform child in characterToShow.transform.parent)
            child.gameObject.SetActive(false);
        characterToShow.SetActive(true);
        lastParent = PauseBtn.transform.parent;
        PauseBtn.transform.SetParent(transform);
        if (GetComponent<Animator>().GetBool("isShow") == false)
            GetComponent<Animator>().SetBool("isShow", true);
        isShow = true;
        TouchPad.SetActive(false);
    }

    public void hide()
    {
        isShow = false;

        if (GetComponent<Animator>().GetBool("isShow"))
            GetComponent<Animator>().SetBool("isShow", false);

        if (audioSource != null)
            audioSource.Stop();

        if (!hasForm)
            TouchPad.SetActive(true);
    }

    public void CheckVoice()
    {
        if (!isShow)
            audioSource.Stop();
    }

    IEnumerator Move(Vector3 startPos, Vector3 endPos, float time)
    {
        float t = 0;
        var rt = GetComponent<RectTransform>();
        while (t < time)
        {
            t += Time.deltaTime;
            rt.anchoredPosition3D = Vector3.Lerp(startPos, endPos, t / time);
            yield return null;
        }
        rt.anchoredPosition3D = endPos;

        if (startPos.x < endPos.x)
            PauseBtn.transform.SetParent(lastParent);
    }
}
