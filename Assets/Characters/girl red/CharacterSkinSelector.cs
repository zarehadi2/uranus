﻿using System.Linq;
using UnityEditor.Animations;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class CharacterSkinSelector : MonoBehaviour
{
    [SerializeField] public CharacterSkin[] characterSkins;

    void Awake()
    {
        var selectedCharacterSkin = characterSkins.FirstOrDefault(skin => skin.selected);
        SetSkin(selectedCharacterSkin);
    }

    public void SelectCharacter(string skinName)
    {

    }

    void SetSkin(CharacterSkin characterSkin)
    {
        print("selected skin name: " + characterSkin.skinName);

        SelectAnimationClip("idle", characterSkin.idleClip);
        SelectAnimationClip("walking", characterSkin.walkClip);
        SelectAnimationClip("jump", characterSkin.jumpClip);

        //characterSkin.animationClips.ToList().ForEach(x =>
        //{
        //    //print(x.stateName);
        //    SelectAnimationClip(x.stateName, x.clip);
        //});
    }

    void SelectAnimationClip(string stateName, AnimationClip animationClip)
    {
        var controller = (AnimatorController)GetComponent<Animator>().runtimeAnimatorController;
        var state = controller.layers[0].stateMachine.states.FirstOrDefault(s => s.state.name.Equals(stateName)).state;
        if (state == null)
        {
            Debug.LogError("Couldn't get the state!");
            return;
        }
        controller.SetStateEffectiveMotion(state, animationClip);
    }
}

[System.Serializable]
public class CharacterSkin
{
    [SerializeField] public bool selected = false;
    [SerializeField] public string skinName;
    //[SerializeField] public CustomAnimationClip[] animationClips;
    [SerializeField] public AnimationClip idleClip;
    [SerializeField] public AnimationClip walkClip;
    [SerializeField] public AnimationClip jumpClip;
}

[System.Serializable]
public class CustomAnimationClip
{
    public string stateName;
    public AnimationClip clip;
}