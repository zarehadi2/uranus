﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class EnemyMovement : MonoBehaviour
{
    [SerializeField] float speed = 1f;
    [SerializeField] bool movingLeft = false;
    //[SerializeField] LayerMask wallMask;
    Collider2D _collider;
    Rigidbody2D _body;

    public AudioSource audioSource;
    public AudioClip walkingSoundEffect;

    void Start()
    {
        _collider = GetComponent<Collider2D>();
        _body = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        CheckFrontRays();
        MoveEnemy();
    }

    void MoveEnemy()
    {
        float dir = movingLeft ? -1f : 1f;
        _body.velocity = new Vector2(dir * speed, _body.velocity.y);
        var scale = transform.localScale;
        if (dir > 0)
        {
            scale.x = -1;
        }
        else
        {
            scale.x = 1;
        }
        transform.localScale = scale;
    }

    float offSet = .01f;
    float distanceToRay = 0.05f;
    void CheckFrontRays()
    {
        float dir = movingLeft ? -1f : 1f;

        var pos = transform.position;
        float distanceX = _collider.bounds.size.x / 2 + offSet;
        float distanceY = _collider.bounds.size.y / 2 + offSet;

        Vector2 startRayTop = new Vector2(pos.x + dir * distanceY, pos.y);
        Vector2 startRayMiddle = new Vector2(pos.x + dir * distanceX, pos.y);

        RaycastHit2D rayTop = Physics2D.Raycast(startRayTop, new Vector2(dir, 0), distanceToRay, LayerMask.GetMask("Ground"));
        RaycastHit2D rayMiddle = Physics2D.Raycast(startRayMiddle, new Vector2(dir, 0), distanceToRay, LayerMask.GetMask("Ground"));

        //Debug.DrawLine(startRayMiddle, startRayMiddle + new Vector2(distanceToRay, 0) * dir);

        var ray = rayMiddle.collider != null ? rayMiddle.collider : rayTop.collider;
        if (rayMiddle.collider != null || rayTop.collider != null)
        {
            //print(transform.name + "\t" + ray.name);
            movingLeft = !movingLeft;
        }
    }

    public void Destroy()
    {
        //Destroy(gameObject);
        GetComponent<Collider2D>().enabled = false;
        enabled = false;
    }

    void OnBecameVisible()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = walkingSoundEffect;
        audioSource.loop = true;
        audioSource.Play();
    }

    void OnBecameInvisible()
    {
        audioSource.Stop();
    }
}
