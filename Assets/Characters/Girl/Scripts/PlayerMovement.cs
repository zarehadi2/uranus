﻿using System.Collections;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour
{
    public enum PlayerState
    {
        idle,
        jumping,
        falling,
        walking,
        bouncing,
        idleClimbing,
        movingClimbing,
    }
    public GameObject PlayerPrefab;
    [Range(0f, 100f)]
    public float Health = 100f;
    [SerializeField] Transform GroundCheck;

    // for checking enemies forward and backward the player
    [SerializeField] Transform SidesCheckLeft;
    [SerializeField] Transform SidesCheckRight;

    // for checking enemies below the player
    [SerializeField] Transform BottomCheckLeft;
    [SerializeField] Transform BottomCheckRight;

    float jumpVelocity = 5.5f;
    float walkSpeed = 1f;
    float timeOfDizzy = 1.5f;
    int maxJump = 2;
    int jumpLefts;
    Animator _anim;

    public PlayerState playerState = PlayerState.idle;

    bool bounce = false;
    bool isDizzy = false;
    bool canJump = false;
    bool isGrounded = false;

    public float H = 0f;
    public float V = 0f;
    public bool jump;

    public float originGravityScale { get; private set; }
    Vector3 _startPos;
    Rigidbody2D _body;

    [HideInInspector] public AudioSource audioSource;
    [SerializeField] AudioClip jumpSoundEffect;
    [SerializeField] AudioClip hitEnemySoundEffect;

    void Awake()
    {
        jumpLefts = maxJump;
        _startPos = transform.position;
        _anim = GetComponent<Animator>();
        _body = GetComponent<Rigidbody2D>();
        audioSource = GetComponent<AudioSource>();
        originGravityScale = _body.gravityScale;
    }

    //bool phone = false;
    bool phone = true;
    void Update()
    {
        CheckEnemySides();

        CheckEnemyBottom();

        CheckInputs();

        CheckGrounded();

        MovePlayer();

        UpdateAnimationStates();
    }

    void FixedUpdate()
    {

    }

    void MovePlayer()
    {

        if (jump && jumpLefts > 0)
        {
            if (audioSource != null)
            {
                audioSource.clip = jumpSoundEffect;
                StartCoroutine(lowMusicPlayerVolume(1f));
                audioSource.Play();
            }

            if (jumpLefts == maxJump && isGrounded)
            {
                _body.velocity = new Vector2(_body.velocity.x, jumpVelocity);
                jumpLefts -= 1;
            }
            else if (jumpLefts < maxJump)
            {
                float jumpForce = jumpVelocity;
                for (int i = 0; i < maxJump - jumpLefts; i++)
                    jumpForce *= 0.66f;
                //print("x= " + jumpForce);
                //_body.AddForce(new Vector2(0, jumpForce));
                _body.velocity = new Vector2(_body.velocity.x, jumpForce);
                print("jumpForce = " + jumpForce);
                jumpLefts -= 1;
            }
            SetState(PlayerState.jumping);
        }
        //print("jumpLefts = " + jumpLefts);
        if (H != 0)
        {
            H = H > 0 ? 1f : -1f;
            _body.velocity = new Vector2(walkSpeed * H, _body.velocity.y);
            SetState(PlayerState.walking);
            var scale = transform.localScale;
            if (H > 0)
            {
                scale.x = 1;
            }
            else
            {
                scale.x = -1;
            }
            transform.localScale = scale;
        }
        else
        {
            if (!jump)
                SetState(PlayerState.idle);
            _body.velocity = new Vector2(0f, _body.velocity.y);
        }
    }

    IEnumerator lowMusicPlayerVolume(float time)
    {
        if (MusicPlayer.instance != null)
        {
            var lastVal = MusicPlayer.instance.audioSource.volume;
            MusicPlayer.instance.audioSource.volume = .7f;

            float t = 0;
            while (t <= time)
            {
                t += Time.deltaTime;
                yield return null;
            }

            MusicPlayer.instance.audioSource.volume = lastVal;
        }
    }

    float timeLeftToCheckGround = 0.2f;
    void CheckGrounded()
    {
        timeLeftToCheckGround -= Time.deltaTime;
        if (timeLeftToCheckGround > 0)
        {
            return;
        }

        var pos = GroundCheck.position;
        var distance = 0.05f;

        Vector2 startRay = new Vector2(pos.x, pos.y);

        RaycastHit2D ray = Physics2D.Raycast(startRay, new Vector2(0, -1f), distance, LayerMask.GetMask("Ground"));

        Debug.DrawLine(startRay, startRay + new Vector2(0, -distance));

        if (ray.collider != null)
        {
            isGrounded = true;
            jumpLefts = maxJump;
            timeLeftToCheckGround = 0.2f;
        }
        else
        {
            isGrounded = false;
        }
    }

    void CheckEnemyBottom()
    {
        var posLeft = BottomCheckLeft.position;
        var posRight = BottomCheckRight.position;
        var distance = 0.05f;

        Vector2 startRayBottomLeft = new Vector2(posLeft.x, posLeft.y);
        Vector2 startRayBottomRight = new Vector2(posRight.x, posRight.y);

        RaycastHit2D rayLeft = Physics2D.Raycast(startRayBottomLeft, new Vector2(0, -1f), distance, LayerMask.GetMask("Enemy"));
        RaycastHit2D rayRight = Physics2D.Raycast(startRayBottomRight, new Vector2(0, -1f), distance, LayerMask.GetMask("Enemy"));

        Debug.DrawLine(startRayBottomLeft, startRayBottomLeft + new Vector2(0, -distance));
        Debug.DrawLine(startRayBottomRight, startRayBottomRight + new Vector2(0, -distance));

        if (rayLeft.collider != null)
        {
            rayLeft.collider.gameObject.GetComponent<EnemyMovement>().Destroy();

            _body.velocity = new Vector2(_body.velocity.x, jumpVelocity * .2f);

            audioSource.clip = hitEnemySoundEffect;
            audioSource.Play();

            // record score
        }
        if (rayRight.collider != null)
        {
            rayRight.collider.gameObject.GetComponent<EnemyMovement>().Destroy();

            _body.velocity = new Vector2(_body.velocity.x, jumpVelocity * .2f);

            audioSource.clip = hitEnemySoundEffect;
            audioSource.Play();

            // record score
        }
    }

    void CheckEnemySides()
    {
        var posLeft = SidesCheckLeft.position;
        var posRight = SidesCheckRight.position;
        var distance = 0.05f;

        Vector2 startRayLeft = new Vector2(posLeft.x, posLeft.y);
        Vector2 startRayRight = new Vector2(posRight.x, posRight.y);

        RaycastHit2D rayLeft = Physics2D.Raycast(startRayLeft, new Vector2(-1f, 0), distance, LayerMask.GetMask("Enemy"));
        RaycastHit2D rayRight = Physics2D.Raycast(startRayRight, new Vector2(1f, 0), distance, LayerMask.GetMask("Enemy"));

        Debug.DrawLine(startRayLeft, startRayLeft + new Vector2(-distance, 0));
        Debug.DrawLine(startRayRight, startRayRight + new Vector2(distance, 0));

        if (rayLeft.collider != null || rayRight.collider != null)
        {
            Respawn();

            // record score
        }
    }

    void CheckInputs()
    {
        if (phone)
        {
            H = CrossPlatformInputManager.GetAxis("Horizontal");
            jump = CrossPlatformInputManager.GetButtonDown("Jump");
        }
        else
        {
            H = Input.GetAxis("Horizontal");
            jump = Input.GetKeyDown(KeyCode.Space);
        }
    }

    void UpdateAnimationStates()
    {
        int state = _anim.GetInteger("state");
        if (playerState == PlayerState.idle && state != 0)
        {
            _anim.SetInteger("state", 0);
        }
        else if (playerState == PlayerState.walking && state != 1)
        {
            _anim.SetInteger("state", 1);
        }
        else if (playerState == PlayerState.falling && state != 2)
        {
            _anim.SetInteger("state", 2);
        }
        else if ((playerState == PlayerState.jumping || playerState == PlayerState.bouncing) && state != 4)
        {
            _anim.SetInteger("state", 4);
        }
    }

    public void TakeDamage(float damage)
    {
        Debug.Log("Taking Damage");
        Health -= damage;
        if (Health <= 0)
        {
            Respawn();
            Debug.Log("Died");
        }
    }

    public void SetState(PlayerState state)
    {
        //Debug.Log("from " + playerState + "\tto " + state);

        //if (playerState == PlayerState.movingClimbing && state == PlayerState.idle)
        //    canJump = true;

        playerState = state;
    }

    public void Respawn()
    {
        if (PlayerPrefab != null)
        {
            Destroy(gameObject);
            var gm = Instantiate(PlayerPrefab, null);

            //gm.transform.position = _startPos;

            gm.transform.position = CheckPointManager.instance.childs[CheckPointManager.instance.checkPointIndex].position;

            gm.name = PlayerPrefab.name;
            foreach (var com in gm.GetComponentsInChildren<Collider2D>())
                com.enabled = true;
            gm.GetComponent<Animator>().enabled = true;
            gm.GetComponent<PlayerMovement>().enabled = true;
            gm.GetComponent<AudioSource>().enabled = true;
            gm.GetComponent<PlayerMovement>()._startPos = _startPos;
            gm.GetComponent<PlayerMovement>().StartDizzy();
        }
        else
        {
            //transform.position = _startPos;
            transform.position = CheckPointManager.instance.childs[CheckPointManager.instance.checkPointIndex].position;

            Health = 100f;
            StartDizzy();
        }
        /*
        LevelManager.instance.heart--;
        Health = 0f;
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (LevelManager.instance.heart > 0)
        {
            Debug.Log("Respawning in checkpoint");
            if (player)
            {
                // set player pos to checkpoint pos
                player.transform.position = CheckPointManager.instance.GetCurrentCheckPointPosition();
                Health = MaxHealth;
            }
            else
            {
                Debug.Log("player not found");
            }
        }
        else
        {
            Debug.Log("Game Over");
            if (player)
            {
                player.SetActive(false);
                if (gameOverMenu)
                {
                    gameControlMenu.SetActive(false);
                    gameOverMenu.SetActive(true);
                }
                else
                {
                    new MenuManager().ReturnToMainMenuScene();
                }
            }
            else
            {
                Debug.Log("player not found");
            }
        }
        */
    }

    void StartDizzy()
    {
        StartCoroutine(BecomingDizzy());
    }
    IEnumerator BecomingDizzy()
    {
        float time = 0f;
        bool vis = true;
        isDizzy = true;
        while (time < timeOfDizzy)
        {
            time += Time.deltaTime;
            gameObject.GetComponent<SpriteRenderer>().enabled = vis;
            vis = !vis;
            yield return 0;
        }
        isDizzy = false;
        gameObject.GetComponent<SpriteRenderer>().enabled = true;
    }

    /*
    void OnCollisionEnter2D(Collision2D target)
    {
        if (target.collider.tag == "Enemy" && !isDizzy)
        {
            if (playerState == PlayerState.walking || playerState == PlayerState.idle)
            {
                TakeDamage(target.collider.GetComponent<EnemyAI>().Damage);
                StartCoroutine(BecomingDizzy());
            }
            else if (playerState == PlayerState.bouncing || playerState == PlayerState.jumping || playerState == PlayerState.falling)
            {
                bounce = true;
                target.collider.GetComponent<EnemyAI>().Crash();
            }
            //target.collider.enabled = false;
        }
    }
    */
}
