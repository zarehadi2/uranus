﻿using UnityEngine;

public class CheckPointOld : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D target)
    {
        if (target.CompareTag("Player"))
        {
            CheckPointManager.instance.checkPointIndex = transform.GetSiblingIndex();
            gameObject.SetActive(false);
        }
    }
}
