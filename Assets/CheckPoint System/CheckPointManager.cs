﻿using System.Collections.Generic;
using UnityEngine;

public class CheckPointManager : MonoBehaviour
{
    public static CheckPointManager instance;
    public int checkPointIndex = 0;
    public List<Transform> childs;

    void Awake()
    {
        instance = this;
        checkPointIndex = 0;
        childs = new List<Transform>();
    }

    void Start()
    {
        foreach (Transform child in transform)
            childs.Add(child);
    }
}
