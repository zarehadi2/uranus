﻿using System.Collections.Generic;
using System.Linq;
using Random = System.Random;

public static class Func
{
    public static T[] Shuffle<T>(T[] array)
    {
        Random rng = new Random();
        int n = array.Length;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = array[k];
            array[k] = array[n];
            array[n] = value;
        }
        return array;
    }

    public static List<T> Shuffle<T>(List<T> list)
    {
        return Shuffle(list.ToArray()).ToList();
    }
}
