﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingPageController : MonoBehaviour
{
    public static string sceneName;

    public float time = 2f;

    void Start()
    {
        StartCoroutine(LoadingLevel());
    }

    IEnumerator LoadingLevel()
    {
        yield return new WaitForSecondsRealtime(time);

        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
        //AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);

        while (!operation.isDone)
        {
            print(operation.progress);
            yield return null;
        }

        //SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());

        print("loaded " + sceneName);
    }
}
