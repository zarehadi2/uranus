﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class RandN : MonoBehaviour
{
    public Text textField;
    public int min, max;
    private bool flag = true;
    public int number, acc;
    public double time = 0;

    void Start()
    {
        if (max == 0) max = 99;
        if (min > max) min = 0;
        number = min;

        acc = (int)Math.Pow(10, Math.Floor(Math.Log10(max) + 1) / 3);
    }

    void Update()
    {
        time += Time.deltaTime;
        if (time > .1f)
        {
            time -= .1f;
            if (flag)
            {
                textField.text = number.ToString();
                number += acc;
                if (number >= max + 1) flag = false;
            }
            else
            {
                textField.text = number.ToString();
                number -= acc;
                if (number <= min - 1) flag = true;
            }
        }
    }
}
