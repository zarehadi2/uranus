﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManagerOld : MonoBehaviour
{

    public Transform LevelListContent;
    public GameObject LevelButtonPrefab;

    void Start()
    {
        FillLevelList();
    }

    public void ExitBtn()
    {
        SceneManager.LoadScene("Main");
    }

    void FillLevelList()
    {
        for (int i = 1; i <= 16; i++)
        {
            var gm = Instantiate(LevelButtonPrefab, LevelListContent) as GameObject;
            //var stars = Random.Range(0, 4);
            if(i > 7)
            {
                gm.GetComponent<LevelButton>().SetData(i, 0, true);
            }
            else
            {
                gm.GetComponent<LevelButton>().SetData(i, (i+1)/2, false);
            }
        }
    }
}
