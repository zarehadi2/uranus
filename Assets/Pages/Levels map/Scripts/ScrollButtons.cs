﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ScrollButtons : MonoBehaviour
{
    public Scrollbar scrollbarH;
    public GameObject content;

    public void OnClick(bool isnext)
    {
        float c_value = content.transform.childCount / 8;
        float acc = 1 / c_value;
        float new_value;
        if (isnext)   new_value = scrollbarH.value + acc;
        else        new_value = scrollbarH.value - acc;
        new_value = Mathf.Clamp(new_value, 0f, 1f);
        //print(scrollbarH.value.ToString() + "\n" + new_value.ToString());
        StartCoroutine(scroll(scrollbarH.value, new_value, .3f));
    }

    IEnumerator scroll(float c_value, float n_value, float time)
    {
        float c_time = 0;
        while(c_time < time)
        {
            c_time += Time.deltaTime;
            scrollbarH.value = Mathf.Lerp(c_value, n_value, c_time / time);
            yield return 0;
        }
    }
}
