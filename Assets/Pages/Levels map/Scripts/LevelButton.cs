﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    public Sprite[] _stars_fill;
    public Sprite[] _stars_empty;
    public Image[] _stars;
    public GameObject _lock;
    public Text _text;

    public void SetData(int _levelNumber, int _stars, bool _lock)
    {
        this._lock.SetActive(_lock);
        for (int i = 0; i < 3; i++)
        {
            this._stars[i].gameObject.SetActive(true);
            this._stars[i].sprite = _stars_empty[i];
        }
        if (!_lock)
        {
            _text.gameObject.SetActive(true);
            _text.text = _levelNumber.ToString();
            for (int i = 0; i < 4 - _stars && i < 3; i++)
                this._stars[i].sprite = _stars_fill[i];
            GetComponent<Button>().onClick.AddListener(delegate {
                LoadingPageController.sceneName = "Level " + _levelNumber;
                SceneManager.LoadScene("Loading"); 
            });
        }
        else
        {
            _text.gameObject.SetActive(false);
        }
    }

}
