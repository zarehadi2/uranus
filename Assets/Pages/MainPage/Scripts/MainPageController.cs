﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainPageController : MonoBehaviour
{
    public void BtnExitClick()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit ();
#endif
    }

    public void BtnShopClick()
    {

    }

    public void BtnStartClick()
    {
        SceneManager.LoadScene("LevelsMap");
    }

    public void BtnMoreGamesClick()
    {

    }

    public void BtnAboutUsClick()
    {

    }
}
