﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PausePageController : MonoBehaviour
{
    public static PausePageController instance;

    public GameObject TopBar;
    public GameObject TouchPad;

    public bool enableTouchPadOrNot = true;

    void Awake()
    {
        if (instance != null)
            Destroy(instance.gameObject);
        instance = this;

        gameObject.SetActive(false);
    }

    public void BtnResume()
    {
        if (enableTouchPadOrNot)
            TouchPad.SetActive(true);

        TopBar.SetActive(true);

        gameObject.SetActive(false);
        Time.timeScale = 1f;

        ShowSpeakingCharacter.instance.CheckVoice();
    }

    public void BtnRetry()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void BtnExit()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Main");

    }
}
