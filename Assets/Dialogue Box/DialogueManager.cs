﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    public static DialogueManager instance;

    public Text nameText, dialogueText, continueText;
    public AudioSource audioSource;
    private Queue<Sentence> sentences;

    void Awake()
    {
        instance = this;
        sentences = new Queue<Sentence>();
    }

    public void StartDialogue(Dialogue dialogue)
    {
        //MusicPlayer.instance.audioSource.volume = 0.2f;
        nameText.text = dialogue.name;
        sentences.Clear();
        foreach (var sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }
        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }
        var sentence = sentences.Dequeue();
        dialogueText.text = sentence.text;
        audioSource.clip = sentence.voice;
        audioSource.loop = false;
        audioSource.Play();
        if (sentences.Count > 0)
            continueText.text = "ادامه";
        else
            continueText.text = "پایان";
    }

    void EndDialogue()
    {
        Time.timeScale = 1f;
        ShowSpeakingCharacter.instance.hide();
        audioSource.Stop();
        //MusicPlayer.instance.audioSource.volume = 1f;
    }
}
/*
 آرگان سفینه شما روی زمین 
سقوط کرده تو باید به دنبال اون شخصی که منابع سیاره شمارو دزدیده بگردی
و باید خانوادتو دراین مسیر پیدا کنی و در این مسیر به هر مانعی که بر خوردی من راهنماییت می کنم
 

آرگان عزیز لطفا پازل سیاره اورانوس را کاملش کن تا بتونی به راحت ادامه بدی

 * */
