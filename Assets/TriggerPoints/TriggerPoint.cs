﻿using UnityEngine;

public class TriggerPoint : MonoBehaviour
{
    public static TriggerPoint instanceActivated;

    public enum TriggerType
    {
        None,
        Puzzle,
        Question
    }

    public TriggerType triggerType = TriggerType.None;

    public GameObject CharacterToShow;
    public GameObject[] ShowOnTrigger;
    public Sprite ShapeAfterTriggered;
    public Sprite ShapeBeforeTriggered;
    public GameObject PuzzleStateBox;

    void Start()
    {
        var sp = GetComponent<SpriteRenderer>();
        if (sp != null && ShapeBeforeTriggered != null)
        {
            sp.sprite = ShapeBeforeTriggered;
        }
    }

    public void TriggerFunc()
    {
        instanceActivated = this;

        var sp = GetComponent<SpriteRenderer>();
        if (sp != null && ShapeAfterTriggered != null)
            sp.sprite = ShapeAfterTriggered;

        foreach (var gm in ShowOnTrigger)
            gm.SetActive(true);

        triggered = true;
        ShowSpeakingCharacter.instance.characterToShow = CharacterToShow;
        ShowSpeakingCharacter.instance.hasForm = triggerType == TriggerType.Puzzle || triggerType == TriggerType.Question;
        ShowSpeakingCharacter.instance.show();
        GetComponent<DialogueTrigger>().TriggerDialogue();
        Time.timeScale = 0f;
    }

    public bool triggered = false;
    void OnTriggerEnter2D(Collider2D target)
    {
        if (triggerType == TriggerType.Puzzle)
            if (TopBarManager.instance.puzzle < TopBarManager.instance.puzzleMax && PuzzleStateBox != null)
            {
                PuzzleStateBox.SetActive(true);
                return;
            }

        if (target.CompareTag("Player"))
        {
            if (triggerType == TriggerType.None)
            {
                if (!triggered)
                    TriggerFunc();
            }
            else
            {
                KeyMenu.instance.menuObject = this;
                KeyMenu.instance.gameObject.SetActive(true);
            }
        }
    }

    void OnTriggerExit2D(Collider2D target)
    {
        if (target.CompareTag("Player"))
        {
            KeyMenu.instance.gameObject.SetActive(false);
            KeyMenu.instance.menuObject = null;
        }
    }

    public void Reset()
    {
        triggered = false;
        var sp = GetComponent<SpriteRenderer>();
        if (sp != null && ShapeBeforeTriggered != null)
        {
            sp.sprite = ShapeBeforeTriggered;
        }
    }
}
