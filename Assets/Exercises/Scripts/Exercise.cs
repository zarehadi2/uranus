﻿using UnityEngine;

public abstract class Exercise : MonoBehaviour
{
    public bool solved = false;
    public float time = 0f;

    protected abstract void CreateNewGame();

    public abstract void OnBtnExit();

    public abstract void OnBtnRetry();

    public abstract void OnBtnSound();
}
