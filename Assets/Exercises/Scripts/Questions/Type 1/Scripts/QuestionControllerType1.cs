﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionControllerType1 : Exercise
{
    public GameObject OptionsPrefab;
    public Transform OptionsParent;
    public Sprite[] OptionsSprite;
    public int answerIndex = -1;
    public bool clickState = false;
    public GameObject enableOnExit;

    private List<GameObject> OptionsObject;

    protected override void CreateNewGame()
    {
        clickState = false;

        if (OptionsObject == null)
            OptionsObject = new List<GameObject>();
        else
        {
            foreach (var g in OptionsObject)
                if (g != null)
                    if (g.gameObject != null)
                        Destroy(g.gameObject);
            OptionsObject.Clear();
        }

        int i = 0;
        foreach (var sp in OptionsSprite)
        {
            var gm = Instantiate(OptionsPrefab, OptionsParent);
            gm.GetComponent<Image>().sprite = sp;
            gm.name = "Option " + i.ToString();
            gm.GetComponent<QuestionControllerType1Option>().index = i;
            gm.SetActive(true);
            OptionsObject.Add(gm);
            i++;
        }

        OptionsObject = Func.Shuffle(OptionsObject);

        for (i = 0; i < OptionsObject.Count; i++)
        {
            OptionsObject[i].transform.SetSiblingIndex(i);
            print(i + " " + OptionsObject[i].name);
        }
    }

    public bool CheckAnswer(int index)
    {
        if (answerIndex == index)
        {
            solved = true;
            return true;
        }
        else
        {
            return false;
        }
    }

    void OnEnable()
    {
        CreateNewGame();
        Time.timeScale = 0f;
        ShowSpeakingCharacter.instance.show();
    }

    void OnDisable()
    {
        Time.timeScale = 1f;
        if (ShowSpeakingCharacter.instance.isShow)
            ShowSpeakingCharacter.instance.hide();
    }

    public override void OnBtnExit()
    {
        gameObject.SetActive(false);

        if (enableOnExit != null)
            enableOnExit.SetActive(true);

        if (!solved)
            TriggerPoint.instanceActivated.Reset();
    }

    public override void OnBtnRetry()
    {
        CreateNewGame();
    }

    public override void OnBtnSound()
    {
        if (!ShowSpeakingCharacter.instance.isShow)
            ShowSpeakingCharacter.instance.show();
        ShowSpeakingCharacter.instance.audioSource.Play();
    }
}
