﻿using System.Collections;
using UnityEngine;

public class QuestionControllerType1Option : MonoBehaviour
{
    public QuestionControllerType1 QuestionParent;
    public GameObject CorrectImg;
    public GameObject WrongImg;
    public int index;

    void OnEnable()
    {
        foreach (Transform child in transform)
            child.gameObject.SetActive(false);
    }

    public void OnClick()
    {
        if (QuestionParent.clickState)
            return;

        QuestionParent.clickState = true;

        var res = QuestionParent.CheckAnswer(index);
        if (res)
        {
            CorrectImg.SetActive(true);

            StartCoroutine(Do(true, 1.5f));
        }
        else
        {
            WrongImg.SetActive(true);

            StartCoroutine(Do(false, 1.5f));
        }
    }

    IEnumerator Do(bool win, float time)
    {
        yield return new WaitForSecondsRealtime(time);
        if (win)
            QuestionParent.OnBtnExit();
        else
            QuestionParent.OnBtnRetry();
    }
}
