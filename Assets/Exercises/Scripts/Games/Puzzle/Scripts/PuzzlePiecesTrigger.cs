﻿using UnityEngine;

public class PuzzlePiecesTrigger : MonoBehaviour
{
    public bool triggered = false;
    AudioSource audioSource;
    public AudioClip soundClip;

    void OnTriggerEnter2D(Collider2D target)
    {
        if (target.CompareTag("Player") && !triggered)
        {
            audioSource = GetComponent<AudioSource>();
            audioSource.clip = soundClip;
            audioSource.Play();
            // LevelManager.collectedPuzzles++;
            GetComponent<SpriteRenderer>().enabled = false;
            Destroy(gameObject, 2f);
            triggered = true;
            TopBarManager.addPuzzle();
        }
    }
}
