﻿using System.Collections.Generic;
using UnityEngine;

public class CellGroup : MonoBehaviour
{
    public int index;

    public List<Cell> cells;

    public void SetData(int index)
    {
        this.index = index;

        cells = new List<Cell>();
        foreach (Cell cell in GetComponentsInChildren<Cell>())
        {
            cells.Add(cell);
        }
    }

    public void UpdateCells()
    {
        cells = new List<Cell>();
        foreach (Cell cell in GetComponentsInChildren<Cell>())
        {
            cells.Add(cell);
        }
    }

    public bool CheckIndex()
    {
        return this.index == cells[0].index;
    }
}
