﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Cell : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public int index;

    public void SetData(int index, Sprite sprite, Vector2 pivot, Vector2 sizeDelta)
    {
        this.index = index;
        GetComponentInChildren<Image>().sprite = sprite;
        foreach (Transform child in transform)
        {
            //print(pivot);
            child.GetComponent<RectTransform>().pivot = pivot;
            child.GetComponent<RectTransform>().sizeDelta = sizeDelta;
        }
    }

    [HideInInspector]
    public Transform parentToReturnTo = null;
    [HideInInspector]
    public Transform placeHolderParent = null;

    public Puzzle PuzzleScript;

    private Vector2 offset;
    private Vector2 startPosition;

    public void OnBeginDrag(PointerEventData eventData)
    {
        startPosition = transform.position;
        offset = eventData.position - startPosition;

        parentToReturnTo = this.transform.parent;
        transform.SetParent(transform.parent.parent);
    }

    public void OnDrag(PointerEventData eventData)
    {
        this.transform.position = eventData.position - offset;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        CellGroup target = null;

        int min = int.MaxValue;
        int cellSize = (int)GetComponent<RectTransform>().sizeDelta.x;
        foreach (var cellGroup in parentToReturnTo.parent.GetComponentsInChildren<CellGroup>())
        {
            if (cellGroup.transform.name == parentToReturnTo.name)
                continue;
            int distance = (int)Vector2.Distance(transform.position, cellGroup.cells[0].transform.position);
            if (distance < cellSize / 2 && distance < min)
            {
                target = cellGroup;
                min = distance;
            }
        }

        if (target == null)
        {
            move(transform, transform.position, startPosition, 0.2f);

            transform.SetParent(parentToReturnTo);
        }
        else
        {
            Cell targetCell = target.cells[0];

            transform.SetParent(target.transform);

            targetCell.transform.SetParent(parentToReturnTo);

            target.UpdateCells();
            parentToReturnTo.GetComponent<CellGroup>().UpdateCells();

            move(transform, transform.position, target.transform.position, 0.2f);
            move(targetCell.transform, targetCell.transform.position, parentToReturnTo.position, 0.2f);
        }

        PuzzleScript.CheckGameState();
    }

    void move(Transform MovingGameObject, Vector2 currentPos, Vector2 targetPos, float time)
    {
        MovingGameObject.position = targetPos;
    }
}
