﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Puzzle : Exercise
{
    public Board board;
    public Sprite img_full;
    public Image img_full_obj;
    public int n;
    public CellGroup cellGroup;
    public List<CellGroup> groups;
    public Point[] indexes;
    public bool gameOver;
    public GameObject disableOnExit;
    public GameObject enableOnExit;

    protected override void CreateNewGame()
    {
        if (groups == null)
            groups = new List<CellGroup>();
        else
        {
            foreach (var g in groups)
                if (g != null)
                    if (g.gameObject != null)
                        Destroy(g.gameObject);
            groups.Clear();
        }

        img_full_obj.sprite = img_full;

        float boardSize = board.gameObject.GetComponent<RectTransform>().sizeDelta.x;

        float cellSize = boardSize / n;

        //print("n=" + n + "\tboardSize=" + boardSize + "\tcellSize=" + cellSize);

        float xPos = 100;
        float yPos = -100;

        //int[] indexes = new int[] { 2, 3, 6, 7, 8, 4, 1, 0, 5 };

        indexes = new Point[(int)(n * n)];
        int ii = 0;
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                indexes[ii] = new Point();
                indexes[ii].x = i;
                indexes[ii].y = j;
                ii++;
            }
        }

        indexes = Func.Shuffle(indexes);

        for (int y = 0; y < n; y++)
        {
            for (int x = 0; x < n; x++)
            {
                int idx = x + y * n;
                var gm = Instantiate(cellGroup.gameObject, board.transform);
                gm.name = "Cell Group " + idx.ToString();
                gm.SetActive(true);
                gm.GetComponent<CellGroup>().SetData(idx);
                // Vector2 pivot = new Vector2(x / n, 1f - (y / n));
                var sizeDelta = new Vector2(boardSize, boardSize);
                gm.GetComponentInChildren<Cell>().SetData(indexes[idx].x + indexes[idx].y * n, img_full, getPivotForIndex(indexes[idx].x, indexes[idx].y, (int)n), sizeDelta);
                gm.transform.localPosition = new Vector3(xPos - 300, yPos + 300, 0);
                groups.Add(gm.GetComponent<CellGroup>());
                xPos += 200;
            }
            yPos -= 200;
            xPos = 100;
        }
    }

    Vector2 getPivotForIndex(int x, int y, int n)
    {
        return new Vector2((float)x / n, 1f - ((float)y / n));
    }

    void OnEnable()
    {
        CreateNewGame();

        Time.timeScale = 0f;
        //print("puzzle enabled");
        disableOnExit.GetComponent<Animator>().SetBool("isShow", true);

        // ShowSpeakingObject.GetComponent<ShowSpeakingCharacter>().show();
        ShowSpeakingCharacter.instance.show();
    }

    void OnDisable()
    {
        Time.timeScale = 1f;

        //print("puzzle disabled");
        // enableOnGameOver[0].GetComponentInChildren<TouchPadInputs>().OnPointerUp(null);
        // disableOnExit.GetComponent<Animator>().SetBool("isShow", false);

        if (ShowSpeakingCharacter.instance.isShow)
            ShowSpeakingCharacter.instance.hide();

        //foreach (var gm in enableOnGameOver)
        //    gm.SetActive(true);
    }

    void Update()
    {
        Time.timeScale = 0f;
    }

    public void CheckGameState()
    {
        var f = true;
        foreach (var g in groups)
        {
            if (g.CheckIndex() == false)
            {
                f = false;
                break;
            }
        }
        if (f)
        {
            solved = true;
            if (enableOnExit != null)
                enableOnExit.SetActive(true);

            if (disableOnExit != null)
                disableOnExit.SetActive(false);
        }
    }

    public override void OnBtnExit()
    {
        if (enableOnExit != null)
            enableOnExit.SetActive(true);

        if (disableOnExit != null)
            disableOnExit.SetActive(false);

        if (!solved)
            TriggerPoint.instanceActivated.Reset();
    }

    public override void OnBtnSound()
    {
        if (!ShowSpeakingCharacter.instance.isShow)
            ShowSpeakingCharacter.instance.show();
        ShowSpeakingCharacter.instance.audioSource.Play();
    }

    public override void OnBtnRetry()
    {
        CreateNewGame();
    }

}

[Serializable]
public class Point
{
    [SerializeField] public int x, y;
}