﻿using UnityEngine;
using UnityEngine.UI;

public class TopBarManager : MonoBehaviour
{
    public static TopBarManager instance;

    public GameObject PuzzleStateBox;

    public int coin { private set; get; } = 0;
    public int coinMax { private set; get; } = 0;
    public int puzzle { private set; get; } = 0;
    public int puzzleMax { private set; get; } = 0;
    public int gem { private set; get; } = 0;
    public int key { private set; get; } = 0;

    public Text coinText, gemText, puzzleText;

    public static void addCoin()
    {
        instance.coin++;
        instance.coinText.text = instance.coin.ToString(); // + "/" + instance.coinMax;
    }

    public static void addPuzzle()
    {
        instance.puzzle++;
        instance.puzzleText.text = instance.puzzle + "/" + instance.puzzleMax;

        if (instance.puzzle == instance.puzzleMax && instance.PuzzleStateBox != null)
            instance.PuzzleStateBox.SetActive(false);
    }

    public static void addGem()
    {
        instance.gem++;
        instance.gemText.text = instance.gem.ToString();
    }

    void Awake()
    {
        instance = this;
        coinMax = FindObjectsOfType<CoinOld>().Length;
        puzzleMax = FindObjectsOfType<PuzzlePiecesTrigger>().Length;
        coinText = GetComponentInChildren<TopBarCoinCount>().GetComponentInChildren<Text>();
        gemText = GetComponentInChildren<TopBarGemCount>().GetComponentInChildren<Text>();
        puzzleText = GetComponentInChildren<TopBarPuzzleCount>().GetComponentInChildren<Text>();
        coinText.text = coin.ToString(); // + "/" + coinMax;
        gemText.text = gem.ToString();
    }

    void Update()
    {

    }
}
