﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

[RequireComponent(typeof(Image))]
public class TouchPadInputs : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public enum AxisOption
    {
        Both,
        OnlyHorizontal,
        OnlyVertical
    }

    public AxisOption axesToUse = AxisOption.Both;

    string Horizontal = "Horizontal";
    string Vertical = "Vertical";
    float minAxisValue = .3f;
    float minDistance = 50f;
    int m_Id;
    bool m_draging = false;
    Vector2 start_point = Vector2.zero;
    bool m_UseX; // Toggle for using the x axis
    bool m_UseY; // Toggle for using the Y axis
    CrossPlatformInputManager.VirtualAxis m_HorizontalVirtualAxis; // Reference to the joystick in the cross platform input
    CrossPlatformInputManager.VirtualAxis m_VerticalVirtualAxis; // Reference to the joystick in the cross platform input

    void Start()
    {
        CreateVirtualAxes();
    }

    void CreateVirtualAxes()
    {
        m_UseX = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyHorizontal);
        m_UseY = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyVertical);

        if (m_UseX)
        {
            m_HorizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(Horizontal);
            CrossPlatformInputManager.RegisterVirtualAxis(m_HorizontalVirtualAxis);
        }
        if (m_UseY)
        {
            m_VerticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(Vertical);
            CrossPlatformInputManager.RegisterVirtualAxis(m_VerticalVirtualAxis);
        }
    }

    void UpdateVirtualAxes(Vector3 value)
    {
        value = value.normalized;
        if (m_UseX)
        {
            m_HorizontalVirtualAxis.Update(value.x);
        }

        if (m_UseY)
        {
            m_VerticalVirtualAxis.Update(value.y);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        m_draging = true;
        m_Id = eventData.pointerId;
#if !UNITY_EDITOR
        start_point = Input.touches[m_Id].position;
#else
        start_point = Input.mousePosition;
#endif
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        m_draging = false;
        UpdateVirtualAxes(Vector3.zero);
    }

    void Update()
    {
        if (m_draging)
        {
            Vector2 delta;
            if (Input.touchCount > 0)
            {
                delta = Input.touches[m_Id].position - start_point;

            }
            else
            {
                Vector2 mouse = Input.mousePosition;
                delta = mouse - start_point;
            }

            delta = normalized(delta);
            if (delta.x < minAxisValue && delta.x > -minAxisValue) delta.x = 0f;
            if (delta.y < minAxisValue && delta.y > -minAxisValue) delta.y = 0f;
            UpdateVirtualAxes(delta);
        }
    }

    void OnDestroy()
    {
        if (CrossPlatformInputManager.AxisExists(Horizontal))
            CrossPlatformInputManager.UnRegisterVirtualAxis(Horizontal);
        if (CrossPlatformInputManager.AxisExists(Vertical))
            CrossPlatformInputManager.UnRegisterVirtualAxis(Vertical);
    }

    Vector2 normalized(Vector2 toBeNormalized)
    {
        if (toBeNormalized.x > minDistance)
            toBeNormalized.x = 1f;
        else if (toBeNormalized.x < -minDistance)
            toBeNormalized.x = -1f;
        else
            toBeNormalized.x = 0f;

        if (toBeNormalized.y > minDistance)
            toBeNormalized.y = 1f;
        else if (toBeNormalized.y < -minDistance)
            toBeNormalized.y = -1f;
        else
            toBeNormalized.y = 0f;

        return toBeNormalized;
    }

    void OnDisable()
    {
        OnPointerUp(null);
    }
}
