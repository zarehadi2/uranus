﻿using UnityEngine;

public class DieArea : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D target)
    {
        if (target.CompareTag("Player"))
            target.GetComponent<PlayerMovement>().Respawn();
        else if (target.CompareTag("Enemy"))
            Destroy(target.gameObject);
    }
}
