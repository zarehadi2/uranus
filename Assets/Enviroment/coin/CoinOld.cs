﻿using UnityEngine;

public class CoinOld : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioClip clip;
    float volume = 1f;

    void OnTriggerEnter2D(Collider2D target)
    {
        if (target.CompareTag("Player"))
        {
            audioSource.PlayOneShot(clip, volume);
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<Collider2D>().enabled = false;
            Destroy(gameObject, 1.5f);
            TopBarManager.addCoin();
        }
    }
}
