﻿using UnityEngine;

public class BackgroundElement : MonoBehaviour
{
    float lastPosX;
    float lastPosY;
    Material material;
    
    protected LevelLimits _levelBounds;
    
    public float speed;

    void Start()
    {
        lastPosX = Camera.main.transform.position.x;
        lastPosY = Camera.main.transform.position.y;

        material = GetComponent<Renderer>().material;
        _levelBounds = GameObject.FindGameObjectWithTag("LevelBounds").GetComponent<LevelLimits>();
    }

    void Update()
    {
        float posX = Camera.main.transform.position.x;
        float posY = Camera.main.transform.position.y;

        Roll(-speed * (lastPosX - posX));

        //_levelBounds.top

        transform.position = new Vector2(posX, transform.position.y);

        lastPosX = posX;
        lastPosY = posY;
    }

    void Roll(float offset)
    {
        material.mainTextureOffset += new Vector2(offset, 0);
    }
}
