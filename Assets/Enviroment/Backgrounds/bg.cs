﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class bg : MonoBehaviour
{
    private Renderer rend;
    private float h = 0;
    private float camWidth, camHeight, levelMinX, levelMaxX;
    private Transform leftBound, rightBound, bottomBound, topBound;
    private GameObject player;
    private Vector2 distance = Vector2.zero;

    void Start()
    {
        if (!leftBound)
            leftBound = GameObject.FindGameObjectWithTag("LeftBound").transform;
        if (!rightBound)
            rightBound = GameObject.FindGameObjectWithTag("RightBound").transform;
        if (!topBound)
            topBound = GameObject.FindGameObjectWithTag("TopBound").transform;
        if (!bottomBound)
            bottomBound = GameObject.FindGameObjectWithTag("BottomBound").transform;

        rend = GetComponent<Renderer>();
        camHeight = Camera.main.orthographicSize * 2;
        camWidth = camHeight * Camera.main.aspect;

        float leftBoundsWidth = leftBound.localScale.x / 2;
        float rightBoundsWidth = rightBound.localScale.x / 2;

        levelMinX = leftBound.position.x + leftBoundsWidth + (camWidth / 2);
        levelMaxX = rightBound.position.x - rightBoundsWidth - (camWidth / 2);

        levelMaxX = float.Parse(levelMaxX.ToString("000.0000"));
        levelMinX = float.Parse(levelMinX.ToString("000.0000"));

        player = GameObject.FindGameObjectWithTag("Player");

        distance = transform.position - player.transform.position;

        playerLastPos = player.transform.position;
    }


    Vector3 playerLastPos;
    bool flag;
    void FixedUpdate()
    {
        if (!player)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }

        flag = false;
        if (playerLastPos == player.transform.position)
            flag = true;
        playerLastPos = player.transform.position;
    }

    void Update()
    {
        if (Time.timeScale == 0f)
            return;
        if (flag)
            return;

        h = Input.GetAxis("Horizontal");
        float CamX = float.Parse(Camera.main.transform.position.x.ToString("000.0000"));
        if (!player)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }

        if (h != 0 && CamX < levelMaxX && CamX > levelMinX)
        {
            Roll(h * .0005f);
        }
        var pos = transform.position;
        pos.x = distance.x + player.transform.position.x;
        pos.x = Camera.main.transform.position.x;
        transform.position = pos;
    }

    void Roll(float offset)
    {
        rend.material.mainTextureOffset += new Vector2(offset, 0);
    }
}
