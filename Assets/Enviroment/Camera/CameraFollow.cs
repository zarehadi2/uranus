﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    private Transform target, leftBound, rightBound, bottomBound, topBound;
    private float smoothDampTime = 0.15f;
    private Vector3 smoothDampVelocity = Vector3.zero;
    private float camWidth, camHeight, levelMinX, levelMaxX, levelMinY, levelMaxY;

    void Start()
    {
        if (!leftBound)
            leftBound = GameObject.FindGameObjectWithTag("LeftBound").transform;
        if (!rightBound)
            rightBound = GameObject.FindGameObjectWithTag("RightBound").transform;
        if (!topBound)
            topBound = GameObject.FindGameObjectWithTag("TopBound").transform;
        if (!bottomBound)
            bottomBound = GameObject.FindGameObjectWithTag("BottomBound").transform;

        if (!target)
            target = GameObject.FindGameObjectWithTag("Player").transform;

        camHeight = Camera.main.orthographicSize * 2;
        camWidth = camHeight * Camera.main.aspect;
        float leftBoundsWidth = leftBound.localScale.x / 2;
        float rightBoundsWidth = rightBound.localScale.x / 2;
        levelMinX = leftBound.position.x + leftBoundsWidth + (camWidth / 2);
        levelMaxX = rightBound.position.x - rightBoundsWidth - (camWidth / 2);
        levelMinY = bottomBound.position.y;
        levelMaxY = topBound.position.y;
    }

    void FixedUpdate()
    {
        if (target)
        {
            float targetX = Mathf.Clamp(target.position.x, levelMinX, levelMaxX);
            //float x = Mathf.SmoothDamp(transform.position.x, targetX, ref smoothDampVelocity.x, smoothDampTime);
            float x = targetX;
            float targetY = Mathf.Clamp(target.position.y, levelMinY, levelMaxY);
            float distance = Mathf.Abs(transform.position.y - target.position.y);

            float y;
            //if (distance > .5f)
                y = Mathf.SmoothDamp(transform.position.y, targetY, ref smoothDampVelocity.y, smoothDampTime);
            //else
            //    y = transform.position.y;
            transform.position = new Vector3(x, y, transform.position.z);
        }
        else
        {
            target = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }
}
