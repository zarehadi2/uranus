﻿using System.Collections;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public MovingPlatformSwitch switchPlatform = null;
    public bool alwaysMove = false;
    public bool loop = false;
    public Vector3[] distances;

    int index = 0;
    bool isMoving = false;
    public float speed = .75f;

    void Update()
    {
        if (alwaysMove && !isMoving)
        {
            StartCoroutine(Move());
        }
    }

    IEnumerator Move()
    {
        isMoving = true;
        if (loop)
        {
            Vector3 firstPos = transform.position;
            Vector3 start = transform.position;
            float dis;
            float time;
            float totalTime;
            while (index < distances.Length)
            {
                Vector3 end = start + distances[index];

                dis = Vector3.Distance(start, end);
                time = 0f;
                totalTime = dis / speed;

                while (time <= totalTime)
                {
                    time += Time.deltaTime;
                    transform.position = Vector3.Lerp(start, end, time / totalTime);
                    yield return 0;
                }

                index++;
                start = transform.position;
            }

            dis = Vector3.Distance(start, firstPos);
            time = 0f;
            totalTime = dis / speed;
            while (time <= totalTime)
            {
                time += Time.deltaTime;
                transform.position = Vector3.Lerp(start, firstPos, time / totalTime);
                yield return 0;
            }
            index = 0;
        }
        else
        {
            Vector3 start = transform.position;
            while (index < distances.Length)
            {
                Vector3 end = start + distances[index];

                float dis = Vector3.Distance(start, end);
                float time = 0f;
                float totalTime = dis / speed;

                while (time <= totalTime)
                {
                    time += Time.deltaTime;
                    transform.position = Vector3.Lerp(start, end, time / totalTime);
                    yield return 0;
                }

                index++;
                start = transform.position;
            }

            index--;

            while (index >= 0)
            {
                Vector3 end = start - distances[index];

                float dis = Vector3.Distance(start, end);
                float time = 0f;
                float totalTime = dis / speed;

                while (time <= totalTime)
                {
                    time += Time.deltaTime;
                    transform.position = Vector3.Lerp(start, end, time / totalTime);
                    yield return 0;
                }
                index--;
                start = transform.position;
            }
            index = 0;
            if (switchPlatform != null)
                switchPlatform.BackTop();
        }
        isMoving = false;
    }

    void OnCollisionEnter2D(Collision2D target)
    {
        if (target.gameObject.CompareTag("Player"))
        {
            target.transform.SetParent(transform);
            if(switchPlatform == null) alwaysMove = true;
        }
    }
    void OnCollisionExit2D(Collision2D target)
    {
        if (target.gameObject.CompareTag("Player"))
        {
            target.transform.SetParent(null);
        }
    }
}
