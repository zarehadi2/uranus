﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatformSwitch : MonoBehaviour
{
    [SerializeField] MovingPlatform platform;
    bool triggered = false;
    void OnTriggerEnter2D(Collider2D target)
    {
        if(target.CompareTag("Player") && !triggered)
        {
            triggered = true;
            platform.alwaysMove = true;
            StartCoroutine(Move(new Vector3(0f, -.5f, 0f)));
        }
    }

    IEnumerator Move(Vector3 distance)
    {
        var pos = transform.position;
        var destination = pos + distance;
        float time = 0f;
        float speed = 1f;
        float total = Vector3.Distance(pos, destination) * speed;
        while(time <= total)
        {
            time += Time.deltaTime;
            transform.position = Vector3.Lerp(pos, destination, time / total);
            yield return 0;
        }
    }

    public void BackTop()
    {
        StartCoroutine(Move(new Vector3(0f, .5f, 0f)));
        platform.alwaysMove = false;
        triggered = false;
    }
}
