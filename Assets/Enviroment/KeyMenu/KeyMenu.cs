﻿using UnityEngine;

public class KeyMenu : MonoBehaviour
{
    public static KeyMenu instance;

    [HideInInspector] public TriggerPoint menuObject;

    void Awake()
    {
        if (instance != null)
            Destroy(instance.gameObject);
        instance = this;
        gameObject.SetActive(false);
    }

    public void OnClick()
    {
        menuObject.TriggerFunc();
        //print(menuObject.name + " clicked");
    }
}
