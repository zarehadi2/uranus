﻿using UnityEngine;

public class StartFlag : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D target)
    {
        //print(target.name + " enters start flag.");
    }

    void OnTriggerExit2D(Collider2D target)
    {
        //print(target.name + " exits start flag.");
    }
}

