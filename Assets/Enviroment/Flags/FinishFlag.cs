﻿using UnityEngine;

public class FinishFlag : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D target)
    {
        FinishMenuController.instance.gameObject.SetActive(true);
        Time.timeScale = 0f;
    }

    void OnTriggerExit2D(Collider2D target)
    {
        print(target.name + " exited");
    }
}
